from config import tokenBot, tokenOwm
from pyowm.utils import timestamps
import requests
import sqlite3
import pyowm
import time

owm = pyowm.OWM(tokenOwm)
mgr = owm.weather_manager()

url = 'https://api.telegram.org/bot{0}/sendMessage'.format(tokenBot)

conn = sqlite3.connect('db.sqlite3', check_same_thread=False)
curs = conn.cursor()


def send_mess(chat_id, mess):
    response = {'chat_id': chat_id, 'text': mess}
    requests.post(url=url, json=response)


while True:
    if time.strftime('%X') == '06:00:00':
        chat_id = curs.execute("SELECT chat_id FROM users").fetchall()
        city = curs.execute("SELECT city FROM users").fetchall()
        for uid, c in zip(chat_id, city):
            if c[0] == 'None':
                pass
            else:
                w = mgr.weather_at_place(c[0]).weather
                temp = w.temperature('celsius')
                send_mess(uid[0], 'Сегодня на улице {0}C°, но ощущается как {1}C°'.format(str(temp['temp']),
                                                                                          str(temp['feels_like'])))
    if time.strftime('%X') == '21:00:00':
        chat_id = curs.execute("SELECT chat_id FROM users").fetchall()
        city = curs.execute("SELECT city FROM users").fetchall()
        for uid, c in zip(chat_id, city):
            if c[0] == 'None':
                pass
            else:
                forecast = mgr.forecast_at_place(c[0], '3h')
                temp = forecast.get_weather_at(timestamps.tomorrow()).temp
                send_mess(uid[0],
                          'Завтра на улице будет {0}C°, но будет ощущается как {1}C°'.format(
                              str(round(temp['temp'] - 273.15, 2)),
                              str(round(temp['feels_like'] - 273.15,
                                        2))))
        time.sleep(0.2)
