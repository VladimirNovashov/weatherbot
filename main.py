from config import tokenBot, tokenOwm
from pyowm.utils import timestamps
from flask import Flask, request
import requests
import sqlite3
import pyowm

app = Flask(__name__)

owm = pyowm.OWM(tokenOwm)
mgr = owm.weather_manager()

url = 'https://api.telegram.org/bot{0}/sendMessage'.format(tokenBot)

conn = sqlite3.connect('db.sqlite3', check_same_thread=False)
curs = conn.cursor()


def get_id(json):
    return json['message']['chat']['id']


def get_text(json):
    return json['message']['text']


def add_user(chat_id):
    user = curs.execute("SELECT chat_id FROM users WHERE chat_id={0}".format(chat_id)).fetchone()
    if user is None:
        curs.execute(
            "INSERT INTO users VALUES ({0}, '{1}')".format(chat_id, 'None'))
        conn.commit()


def send_mess(chat_id, mess, names_btn=[]):
    if len(names_btn) == 0:
        response = {'chat_id': chat_id, 'text': mess}
        requests.post(url=url, json=response)
    elif len(names_btn) != 0:
        buttons = [[{'text': names_btn[0]}],
                   [{'text': names_btn[1]}]]
        response = {'chat_id': chat_id, 'text': mess,
                    'reply_markup': {'keyboard': buttons, 'resize_keyboard': True}}
        requests.post(url=url, json=response)


@app.route('/', methods=['POST', 'GET'])
def text():
    if request.method == "POST":
        json = request.get_json()
        chat_id = get_id(json)
        txt = get_text(json)

        # Команда /start
        if txt == '/start':
            send_mess(chat_id=chat_id,
                      mess='Привет {0} ты запустил WeatherBot\n\n'
                           'Моя задача присылать тебе прогнозы погоды\n\n'
                           ''
                           'Удачи в использовании!!! ☺\n\n'.format(json['message']['chat']['first_name']),
                      names_btn=['Погода на завтра', 'Погода сейчас'])
            add_user(chat_id)
            send_mess(chat_id, 'Для начала работы введите город')

        # Вывести погоду сейчас
        elif txt == 'Погода сейчас':
            add_user(chat_id=chat_id)
            city = curs.execute("SELECT city FROM users WHERE chat_id={0}".format(chat_id)).fetchone()[0]
            if city == 'None':
                send_mess(chat_id, 'Выбирете город')
            else:
                w = mgr.weather_at_place(city).weather
                temp = w.temperature('celsius')
                send_mess(chat_id, 'Сейчас на улице {0}C°, но ощущается как {1}C°'.format(str(temp['temp']),
                                                                                          str(temp['feels_like'])))

        # Вывести погоду на завтра
        elif txt == 'Погода на завтра':
            add_user(chat_id=chat_id)
            city = curs.execute("SELECT city FROM users WHERE chat_id={0}".format(chat_id)).fetchone()[0]
            if city == 'None':
                send_mess(chat_id, 'Выбирете город')
            else:
                forecast = mgr.forecast_at_place(city, '3h')
                temp = forecast.get_weather_at(timestamps.tomorrow()).temp
                send_mess(chat_id,
                          'Завтра на улице будет {0}C°, но будет ощущается как {1}C°'.format(
                              str(round(temp['temp'] - 273.15, 2)),
                              str(round(temp['feels_like'] - 273.15,
                                        2))))

        elif True:
            try:
                mgr.weather_at_place(txt).weather
            except pyowm.commons.exceptions.NotFoundError:
                send_mess(chat_id, 'Город не найден\n'
                                   'Повторите попытку')
                return {'ok': True}
            curs.execute(
                "UPDATE users SET city='{0}' WHERE chat_id={1}".format(txt, chat_id))
            conn.commit()
            send_mess(chat_id, 'OK, Город установлен\n'
                               'Чтобы устаносить другой город просто наберите его в чате')

    return {'ok': True}


app.run(host='192.168.1.106', port=5000)
